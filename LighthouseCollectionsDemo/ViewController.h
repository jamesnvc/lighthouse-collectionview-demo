//
//  ViewController.h
//  LighthouseCollectionsDemo
//
//  Created by James Cash on 19-08-15.
//  Copyright (c) 2015 Occasionally Cogent. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;


@end

