//
//  ViewController.m
//  LighthouseCollectionsDemo
//
//  Created by James Cash on 19-08-15.
//  Copyright (c) 2015 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"
#import "CircularLayout.h"

@interface ViewController () {
    UICollectionViewFlowLayout *flowLayout;
    UICollectionViewFlowLayout *smallLayout;
    CircularLayout *circularLayout;
}

@end

@implementation ViewController


- (IBAction)toggleLayout:(id)sender {
    UICollectionViewLayout *nextLayout;
    if (self.collectionView.collectionViewLayout == flowLayout) {
        nextLayout = smallLayout;
    } else if (self.collectionView.collectionViewLayout == smallLayout) {
        nextLayout = circularLayout;
    } else {
        nextLayout = flowLayout;
    }
    [self.collectionView.collectionViewLayout invalidateLayout];
    [self.collectionView setCollectionViewLayout:nextLayout animated:YES];
}

#pragma mark - Lifecycle

- (void)viewDidLoad {
    [super viewDidLoad];

    flowLayout = [[UICollectionViewFlowLayout alloc] init];
    flowLayout.itemSize = CGSizeMake(250, 140);
    flowLayout.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10);
    flowLayout.minimumInteritemSpacing = 15;
    flowLayout.minimumLineSpacing = 10;
    flowLayout.headerReferenceSize = CGSizeMake(self.collectionView.frame.size.width, 100);

    smallLayout = [[UICollectionViewFlowLayout alloc] init];
    smallLayout.itemSize = CGSizeMake(140, 100);
    smallLayout.sectionInset = UIEdgeInsetsMake(5, 5, 5, 5);
    smallLayout.minimumLineSpacing = 5;
    smallLayout.minimumInteritemSpacing = 5;
    smallLayout.headerReferenceSize = CGSizeMake(CGRectGetWidth(self.collectionView.frame), 75);

    circularLayout = [[CircularLayout alloc] init];

    self.collectionView.collectionViewLayout = flowLayout;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UICollectionViewDataSource

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 5;
}

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 3;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"demoCell" forIndexPath:indexPath];
    cell.backgroundColor = [UIColor redColor];
    UILabel *label = (UILabel*)[cell viewWithTag:1];
    label.text = [NSString stringWithFormat:@"%ld/%ld", (long)indexPath.section, (long)indexPath.item];
    return cell;
}

-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        UICollectionReusableView *header = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"demoHeader" forIndexPath:indexPath];
        UILabel *label = (UILabel*)[header viewWithTag:1];
        label.text = [NSString stringWithFormat:@"Begin %ld", (long)indexPath.section];
        return header;
    }
    if ([kind isEqualToString:UICollectionElementKindSectionFooter]) {
        UICollectionReusableView *footer = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"demoFooter" forIndexPath:indexPath];
        UILabel *label = (UILabel*)[footer viewWithTag:1];
        label.text = [NSString stringWithFormat:@"End %ld", (long)indexPath.section];
        return footer;
    }
    return nil;
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section
{
    CGFloat width = CGRectGetWidth(self.collectionView.frame);
    switch (section) {
        case 0:
            return CGSizeMake(width, 100);
            break;
        case 1:
            return CGSizeMake(width, 75);
            break;
        default:
            return CGSizeMake(width, 50);
            break;
    }
}

@end
